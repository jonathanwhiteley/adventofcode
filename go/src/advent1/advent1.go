package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

type operation struct {
	operand byte
	number  int64
}

func main() {

	file, err := os.Open("/Users/jonathanwhiteley/Projects/AdventOfCode/go/src/advent1/input1.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var currentNumber int64
	currentNumber = 0

	operations := make([]operation, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		currentLine := scanner.Text()
		number := currentLine[1:]
		operator := currentLine[0]
		var numberAsNumber int64
		numberAsNumber, _ = strconv.ParseInt(number, 0, 64)

		op := operation{operand: operator, number: numberAsNumber}

		operations = append(operations, op)

		// numberAsNumber, _ = strconv.ParseInt(number, 0, 64)

		// if operator == '+' {
		// 	currentNumber = currentNumber + numberAsNumber
		// }

		// if operator == '-' {

		// 	currentNumber = currentNumber - numberAsNumber
		// }

	}

	frequencyMap := make(map[int64]bool)
	frequencyMap[0] = true
	count := 0

	for true {
		for _, op := range operations {
			if op.operand == '+' {
				currentNumber = currentNumber + op.number
			}
			if op.operand == '-' {
				currentNumber = currentNumber - op.number
			}

			if frequencyMap[currentNumber] {
				log.Println(count)
				log.Fatal(currentNumber)
			} else {
				frequencyMap[currentNumber] = true
			}
		}

		count++
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
