package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type star struct {
	startX    int
	startY    int
	velocityX int
	velocityY int
}

func (myStar star) xAtGeneration(generation int) int {
	return myStar.startX + (generation * myStar.velocityX)
}

func (myStar star) yAtGeneration(generation int) int {
	return myStar.startY + (generation * myStar.velocityY)
}

type boundingBox struct {
	minX int
	minY int
	maxX int
	maxY int
}

func intAbs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func (bBox boundingBox) printableGrid() [][]string {

	length := intAbs(bBox.maxX-bBox.minX) + 1
	height := intAbs(bBox.maxY-bBox.minY) + 1

	grid := make([][]string, height)

	for i := 0; i < height; i++ {
		grid[i] = make([]string, length)

		for j := 0; j < length; j++ {
			grid[i][j] = " "
		}
	}

	return grid
}

func (bBox boundingBox) area() int {
	length := intAbs(bBox.maxX - bBox.minX)
	height := intAbs(bBox.maxY - bBox.minY)
	return length * height
}

func starFromCoords(coords []string) star {

	startX, _ := strconv.Atoi(coords[0])
	startY, _ := strconv.Atoi(coords[1])
	velocityX, _ := strconv.Atoi(coords[2])
	velocityY, _ := strconv.Atoi(coords[3])

	return star{startX: startX, startY: startY, velocityX: velocityX, velocityY: velocityY}
}

func buildLineInput(filename string) []star {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	stars := make([]star, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		currentLine := scanner.Text()

		re := regexp.MustCompile("-?[0-9]+")

		coords := re.FindAllString(currentLine, -1)

		stars = append(stars, starFromCoords(coords))
	}

	return stars
}

func findBoundingBoxAtGeneration(stars []star, generation int) boundingBox {

	bBox := boundingBox{minX: 0, minY: 0, maxX: 0, maxY: 0}

	for index, myStar := range stars {
		starX := myStar.xAtGeneration(generation)
		starY := myStar.yAtGeneration(generation)

		if index == 0 {
			bBox.minX = starX
			bBox.maxX = starX
			bBox.minY = starY
			bBox.maxY = starY
		} else {
			if bBox.minX > starX {
				bBox.minX = starX
			}

			if bBox.maxX < starX {
				bBox.maxX = starX
			}
			if bBox.minY > starY {
				bBox.minY = starY
			}

			if bBox.maxY < starY {
				bBox.maxY = starY
			}

		}
	}

	return bBox
}

func main() {

	stars := buildLineInput(os.Args[1])

	log.Println(len(stars))

	initialBox := findBoundingBoxAtGeneration(stars, 0)

	currentArea := initialBox.area()

	log.Println(currentArea)

	atMinimum := false

	generation := 0

	for !atMinimum {
		generation++

		nextBox := findBoundingBoxAtGeneration(stars, generation)
		nextArea := nextBox.area()

		if currentArea >= nextArea {
			currentArea = nextArea
		} else {
			atMinimum = true
		}
	}

	correctGen := generation - 1

	fmt.Println(correctGen)

	correctBox := findBoundingBoxAtGeneration(stars, correctGen)

	outputGrid := correctBox.printableGrid()

	for _, myStar := range stars {
		xStar := myStar.xAtGeneration(correctGen)
		yStar := myStar.yAtGeneration(correctGen)

		// log.Println(" (", xStar, ",", yStar, ")")
		// log.Println(" (", correctBox.minX, ",", correctBox.minY, ")")
		// log.Println(" (", correctBox.maxX, ",", correctBox.maxY, ")")

		outputGrid[yStar-correctBox.minY][xStar-correctBox.minX] = "*"
	}

	for _, line := range outputGrid {
		fmt.Println(line)
	}

}
