package main

import (
	"fmt"
	"os"
	"strconv"
)

func findHundredth(input int) int {
	return (input / 100) % 10
}

func make2DArray(x int, y int) [][]int {

	grid := make([][]int, x)

	for i := 0; i < x; i++ {
		grid[i] = make([]int, y)
	}

	return grid
}

func make2DArrayBool(x int, y int) [][]bool {

	grid := make([][]bool, x)

	for i := 0; i < x; i++ {
		grid[i] = make([]bool, y)
	}

	return grid
}

func calcValueAt(x int, y int, serialNumber int) int {

	rackId := x + 10

	power := y * rackId

	power += serialNumber

	power *= rackId

	value := findHundredth(power)

	return value - 5

}

func main() {

	inputNumber, _ := strconv.Atoi(os.Args[1])

	fmt.Println("power level ", inputNumber)

	fmt.Println(calcValueAt(122, 79, 57))

	maxX := 300
	maxY := 300

	precalc := make2DArray(maxX+1, maxY+1)
	hasPreCalc := make2DArrayBool(maxX+1, maxY+1)

	bestX := -1
	bestY := -1

	bestArea := -5000

	for y := 1; y < (maxY - 3); y++ {
		for x := 1; x < (maxX - 3); x++ {

			area := 0

			for relY := 0; relY < 3; relY++ {
				curY := relY + y

				for relX := 0; relX < 3; relX++ {
					curX := relX + x

					if !hasPreCalc[curY][curX] {
						precalc[curY][curX] = calcValueAt(curX, curY, inputNumber)
						hasPreCalc[curY][curX] = true
					}

					area += precalc[curY][curX]
				}
			}

			if area > bestArea {
				bestArea = area

				bestY = y
				bestX = x
			}
		}
	}

	fmt.Println("( ", bestX, " , ", bestY, ") being ", bestArea)

	bestArea2 := -5000
	bestX2 := -1
	bestY2 := -1
	bestSize := -1

	for size := 1; size < maxX; size++ {
		fmt.Println("Doing size ", size)
		for y := 1; y < maxY-size; y++ {
			for x := 1; x < maxX-size; x++ {
				area := 0

				for relY := 0; relY < size; relY++ {
					curY := relY + y

					for relX := 0; relX < size; relX++ {
						curX := relX + x

						if !hasPreCalc[curY][curX] {
							precalc[curY][curX] = calcValueAt(curX, curY, inputNumber)
							hasPreCalc[curY][curX] = true
						}

						area += precalc[curY][curX]
					}
				}

				if area > bestArea2 {
					bestArea2 = area

					bestY2 = y
					bestX2 = x
					bestSize = size
				}

			}
		}
	}

	fmt.Println("( ", bestX2, " , ", bestY2, ", ", bestSize, ") being ", bestArea2)

}
