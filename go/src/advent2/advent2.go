package main

import (
	"bufio"
	"log"
	"os"
)

func buildLineInput(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		lines = append(lines, scanner.Text())
	}

	return lines
}

func mapContainsFrequncey(mapToCheck map[rune]int64, frequency int64) bool {
	for _, value := range mapToCheck {
		if value == frequency {
			return true
		}
	}

	return false

}

func compareStringsForDifference(first []rune, second []rune) (bool, string) {
	diffIndex := -1

	if len(first) != len(second) {
		return false, ""
	}

	for index, charac := range first {
		if charac != second[index] {
			if diffIndex == -1 {
				diffIndex = index
			} else {
				//Differs by more than 1
				return false, ""
			}
		}
	}

	if diffIndex != -1 {
		left := first[:diffIndex]
		right := first[diffIndex+1:]
		both := append(left, right...)
		return true, string(both)
	}

	return false, ""

}

func main() {

	boxIds := buildLineInput(os.Args[1])

	twoCount := 0
	threeCount := 0

	for _, boxID := range boxIds {
		charArray := []rune(boxID)

		countMap := make(map[rune]int64)

		for _, charac := range charArray {
			if value, exist := countMap[charac]; exist {
				countMap[charac] = value + 1
			} else {
				countMap[charac] = 1
			}
		}

		if mapContainsFrequncey(countMap, 2) {
			twoCount = twoCount + 1
		}

		if mapContainsFrequncey(countMap, 3) {
			threeCount = threeCount + 1
		}
	}

	checksum := twoCount * threeCount

	log.Println(checksum)

	for index, boxID := range boxIds {
		for _, otherBoxID := range boxIds[index+1:] {
			match, value := compareStringsForDifference([]rune(boxID), []rune(otherBoxID))

			if match {
				log.Println(value)
				return
			}
		}
	}
}
