package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type fabric struct {
	index   int64
	xStart  int64
	yStart  int64
	xEnd    int64
	yEnd    int64
	xLength int64
	yLength int64
}

func getIntFromString(input string) int64 {
	numberAsNumber, _ := strconv.ParseInt(input, 0, 64)
	return numberAsNumber
}

func buildFabricFromLine(line string) fabric {
	components := strings.Fields(line)
	index := getIntFromString(components[0][1:])

	coords := strings.Split(components[2], ",")
	xStart := getIntFromString(coords[0])
	yStart := getIntFromString(strings.TrimSuffix(coords[1], ":"))

	dimensions := strings.Split(components[3], "x")
	xLength := getIntFromString(dimensions[0])
	yLength := getIntFromString(dimensions[1])

	xEnd := xStart + xLength
	yEnd := yStart + yLength

	return fabric{index: index, xStart: xStart, yStart: yStart, xEnd: xEnd, yEnd: yEnd, xLength: xLength, yLength: yLength}

}

func buildLineInput(filename string) ([]fabric, int64, int64, map[int]bool) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	lines := make([]fabric, 0)
	var maxX, maxY int64
	maxX = 0
	maxY = 0

	collsionMap := make(map[int]bool)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fabric := buildFabricFromLine(scanner.Text())

		if fabric.xEnd > maxX {
			maxX = fabric.xEnd
		}

		if fabric.yEnd > maxY {
			maxY = fabric.yEnd
		}

		collsionMap[int(fabric.index)] = true
		lines = append(lines, fabric)
	}

	return lines, maxX, maxY, collsionMap
}

func make2DArray(x int64, y int64) [][]int {
	x32 := int(x)
	y32 := int(y)
	grid := make([][]int, x)

	for i := 0; i < x32; i++ {
		grid[i] = make([]int, y32)
	}

	return grid
}

func main() {

	fabrics, maxX, maxY, collisionMap := buildLineInput(os.Args[1])

	roll := make2DArray(maxX, maxY)
	collisionRoll := make2DArray(maxX, maxY)
	collisionCount := 0

	for _, fabric := range fabrics {
		for i := fabric.xStart; i < fabric.xEnd; i++ {
			for j := fabric.yStart; j < fabric.yEnd; j++ {
				if roll[int(i)][int(j)] > 0 {
					if roll[int(i)][int(j)] == 1 {

						collisionCount++
					}
					collisionMap[collisionRoll[int(i)][int(j)]] = false
					collisionMap[int(fabric.index)] = false
				}

				roll[int(i)][int(j)] = roll[int(i)][int(j)] + 1
				collisionRoll[int(i)][int(j)] = int(fabric.index)

			}
		}
	}

	log.Println(collisionCount)

	for index, hasNoCollision := range collisionMap {
		if hasNoCollision {
			log.Println(index)
		}
	}

}
