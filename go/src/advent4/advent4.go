package main

import (
	"strings"
	// "fmt"
	// "time"
	"bufio"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
)

const dateFormat = "2006-01-02 15:04"

func buildLineInput(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		lines = append(lines, scanner.Text())
	}

	return lines
}

func extractGuardReference(logRecord string) int {
	guardIdRegex := regexp.MustCompile("#\\d+")

	guardId := guardIdRegex.FindAllString(logRecord, -1)[0]

	return justGiveMeAFreakingIntFromThisString(guardId[1:])

}

func justGiveMeAFreakingIntFromThisString(input string) int {
	trimmed := strings.TrimLeft(input, "0")

	number, _ := strconv.ParseInt(trimmed, 0, 64)

	return int(number)
}

func main() {

	loggedRecords := buildLineInput(os.Args[1])

	sort.Strings(loggedRecords)

	timeStampRegex := regexp.MustCompile(`\[([^\[\]]*)\]`)

	currentGuardId := -1
	sleepStartMinute := -1

	guardElapsedMins := make(map[int][]int)
	guardAsleepTotal := make(map[int]int)
	guardAsleepTotal2 := make(map[int]int)

	for _, logRecord := range loggedRecords {
		matches := timeStampRegex.FindAllString(logRecord, -1)
		logTime := matches[0]

		//log.Println(logTime)

		if strings.Contains(logRecord, "Guard") {

			if sleepStartMinute > 0 {
				log.Println("Hanging sleep minute for ", currentGuardId, " at ", sleepStartMinute)
			}

			currentGuardId = extractGuardReference(logRecord)

			if _, exists := guardElapsedMins[currentGuardId]; !exists {
				guardElapsedMins[currentGuardId] = make([]int, 60)
				guardAsleepTotal[currentGuardId] = 0
				guardAsleepTotal2[currentGuardId] = 0
			}
		} else if strings.Contains(logRecord, "asleep") {
			// log.Println(strings.Split(logTime, ":")[1])
			logMinute := strings.Split(logTime, ":")[1][:2]
			sleepStartMinute = justGiveMeAFreakingIntFromThisString(logMinute)
		} else if strings.Contains(logRecord, "wakes") {
			// log.Println(strings.Split(logTime, ":")[1])
			logMinute := strings.Split(logTime, ":")[1][:2]

			sleepEndMinute := justGiveMeAFreakingIntFromThisString(logMinute)

			// log.Println("guard ", currentGuardId, " asleep between ", sleepStartMinute, " and ", sleepEndMinute)

			for i := sleepStartMinute; i < sleepEndMinute; i++ {
				guardElapsedMins[currentGuardId][i] = guardElapsedMins[currentGuardId][i] + 1
				guardAsleepTotal[currentGuardId] = guardAsleepTotal[currentGuardId] + 1
			}

			guardAsleepTotal2[currentGuardId] = guardAsleepTotal2[currentGuardId] + (sleepEndMinute - sleepStartMinute)

			if guardAsleepTotal[currentGuardId] != guardAsleepTotal2[currentGuardId] {
				log.Println(logRecord, " ", guardAsleepTotal[currentGuardId], " ", guardAsleepTotal2[currentGuardId])
				log.Println("guard ", currentGuardId, " asleep between ", sleepStartMinute, " and ", sleepEndMinute)
			}

			sleepStartMinute = -1

		}
	}

	//find best guard id
	bestGuardId := -1
	mostAsleepMinutes := -1

	for guardId, sleepTime := range guardAsleepTotal {
		// log.Println(guardId, " : ", sleepTime, " : ", guardAsleepTotal2[guardId])
		if guardAsleepTotal2[guardId] > mostAsleepMinutes {
			log.Println(sleepTime, " > ", mostAsleepMinutes)
			bestGuardId = guardId
			mostAsleepMinutes = guardAsleepTotal2[guardId]
		}
	}

	bestMinute := -1
	bestFreqenucy := -1

	for minute, frequency := range guardElapsedMins[bestGuardId] {

		if frequency > bestFreqenucy {
			bestFreqenucy = frequency
			bestMinute = minute
		}
	}

	//Part 2
	bestGuardId2 := -1

	bestMinute2 := -1
	bestFreqenucy2 := -1

	for guardId, sleepTimes := range guardElapsedMins {
		guardMostAsleepMinute := -1
		guardMostAsleepFreqency := -1

		for minute, frequency := range sleepTimes {
			if frequency > guardMostAsleepFreqency {
				guardMostAsleepFreqency = frequency
				guardMostAsleepMinute = minute
			}
		}

		if guardMostAsleepFreqency > bestFreqenucy2 {
			bestGuardId2 = guardId
			bestMinute2 = guardMostAsleepMinute
			bestFreqenucy2 = guardMostAsleepFreqency
		}
	}

	log.Println("Guard Id: ", bestGuardId, " Minute: ", bestMinute, " Frequncy: ", bestFreqenucy, " output: ", (bestGuardId * bestMinute))

	log.Println("Guard Id: ", bestGuardId2, " Minute: ", bestMinute2, " Frequncy: ", bestFreqenucy2, " output: ", (bestGuardId2 * bestMinute2))

}
