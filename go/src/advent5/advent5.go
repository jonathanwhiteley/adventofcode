package main

import (
	"strings"
	// "fmt"
	// "time"
	"bufio"
	"log"
	"os"
	"regexp"
	// "sort"
	// "strconv"
)

func buildLineInput(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		lines = append(lines, scanner.Text())
	}

	return lines
}

func areStringsDifferentCaseButSameValue(left string, right string) bool {
	leftLower := strings.ToLower(left)
	rightLower := strings.ToLower(right)

	return (leftLower == rightLower) && (left != right)
}

func getLastCharacterOfString(input string) string {
	if len(input) == 0 {
		return input
	}

	return string(input[len(input)-1:])
}

func findOccurencesOfCharcterInString(input string, character string) int {
	re := regexp.MustCompile("(?i)(" + character + ")")

	return len(re.FindAllString(input, -1))
}

func removeAllOccurencesOfCharacterFromString(input string, character string) string {
	re := regexp.MustCompile("(?i)(" + character + ")")

	return re.ReplaceAllString(input, "")
}

func uniqueCharactersInString(input string) []string {
	uniques := make([]string, 0)
	existMap := make(map[string]bool)

	for _, charcter := range input {
		current := strings.ToLower(string(charcter))

		if _, exists := existMap[current]; !exists {
			existMap[current] = true
			uniques = append(uniques, current)
		}
	}

	return uniques
}

func reactPolymer(polymer string) string {
	outputChain := ""

	for _, charcter := range polymer {
		if len(outputChain) == 0 {
			outputChain = outputChain + string(charcter)
		} else {
			topOfOutput := getLastCharacterOfString(outputChain)
			inputChar := string(charcter)

			if areStringsDifferentCaseButSameValue(topOfOutput, inputChar) {
				outputChain = outputChain[:(len(outputChain) - 1)]
			} else {
				outputChain = outputChain + inputChar
			}
		}

	}

	return outputChain
}

func main() {

	input := buildLineInput(os.Args[1])

	if len(input) != 1 {
		log.Fatal("Not only 1 line of input")
	}

	polymer := input[0]

	uniques := uniqueCharactersInString(polymer)

	log.Println("Input lenghth:", len(polymer))

	outputChain := reactPolymer(polymer)

	// log.Println(outputChain)
	log.Println(len(outputChain))

	smallestLength := len(polymer)
	bestChar := "blah"

	for _, unique := range uniques {
		smallerPolymer := removeAllOccurencesOfCharacterFromString(polymer, unique)

		reacted := reactPolymer(smallerPolymer)

		reactedLen := len(reacted)

		if reactedLen < smallestLength {
			smallestLength = reactedLen
			bestChar = unique
		}
	}

	log.Println(bestChar)
	log.Println(smallestLength)
}
