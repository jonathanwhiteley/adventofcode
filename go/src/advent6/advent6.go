package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type coordinate struct {
	index int
	x     int
	y     int
}

func justGiveMeAFreakingIntFromThisString(input string) int {
	trimmed := strings.TrimLeft(input, "0")

	number, _ := strconv.ParseInt(trimmed, 0, 64)

	return int(number)
}

func buildLineInput(filename string) []coordinate {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	coords := make([]coordinate, 0)
	index := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		line := scanner.Text()

		lineSplit := strings.Split(line, ", ")

		corod := coordinate{x: justGiveMeAFreakingIntFromThisString(lineSplit[0]), y: justGiveMeAFreakingIntFromThisString(lineSplit[1]), index: index}

		coords = append(coords, corod)
		index++
	}

	return coords
}

func maxesFromCoords(coords []coordinate) (int, int) {
	maxX := -1
	maxY := -1

	for _, coord := range coords {

		if coord.x > maxX {
			maxX = coord.x
		}

		if coord.y > maxY {
			maxY = coord.y
		}

	}

	return maxX + 1, maxY + 1
}

func intAbs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func manhattenDistance(first coordinate, second coordinate) int {
	// log.Println("manhat from (", first.x, ",", first.y, ") and (", second.x, ",", second.y, ")")
	ans := intAbs(first.x-second.x) + intAbs(first.y-second.y)
	// log.Println(ans)
	return ans
}

func make2DArray(x int, y int) [][]string {
	grid := make([][]string, x)

	for i := 0; i < x; i++ {
		grid[i] = make([]string, y)
	}

	return grid
}

func make2DArrayOfInt(x int, y int) [][]int {
	grid := make([][]int, x)

	for i := 0; i < x; i++ {
		grid[i] = make([]int, y)
	}

	return grid
}

func main() {
	coords := buildLineInput(os.Args[1])
	radix, _ := strconv.Atoi(os.Args[2])

	log.Println(len(coords))

	maxX, maxY := maxesFromCoords(coords)

	log.Println(maxX, " , ", maxY)

	nearestCounts := make(map[int]int)
	touchesTheWalls := make(map[int]bool)

	grid := make2DArray(maxX, maxY)
	totalLessThanCount := 0

	for x := 0; x < maxX; x++ {
		for y := 0; y < maxY; y++ {
			currentLowest := maxX + maxY
			bestIndex := make([]int, 0)
			currentCoord := coordinate{x: x, y: y, index: -1}
			totalDistance := 0

			for _, coord := range coords {
				distance := manhattenDistance(currentCoord, coord)
				totalDistance += distance

				if distance < currentLowest {
					// log.Println(distance, " is lower than ", currentLowest)
					bestIndex = make([]int, 0)
					bestIndex = append(bestIndex, coord.index)
					currentLowest = distance
				} else if distance == currentLowest {
					// log.Println(distance, " is equal to ", currentLowest)
					bestIndex = append(bestIndex, coord.index)
					// log.Println(bestIndex)
				}

			}

			// log.Println("(", x, ",", y, ") best : ", bestIndex)

			if len(bestIndex) == 1 {
				bestestIndex := bestIndex[0]
				// log.Println(bestestIndex, " : (", x, ",", y, ")")
				nearestCounts[bestestIndex] = nearestCounts[bestestIndex] + 1
				grid[x][y] = strconv.Itoa(bestestIndex)

				if x == 0 || y == 0 || x == (maxX-1) || y == (maxY-1) {
					// log.Println("(", x, ",", y, ") touches the walls")
					touchesTheWalls[bestestIndex] = true
				}
			} else {
				grid[x][y] = "."
			}

			if totalDistance < radix {
				totalLessThanCount++
			}
		}
	}

	highestCount := -1
	mostAwesomeIndex := -1

	for index, count := range nearestCounts {
		if count > highestCount && !touchesTheWalls[index] {
			highestCount = count
			mostAwesomeIndex = index
		}
	}

	// for _, row := range grid {
	// 	log.Println(row)
	// }

	// log.Println(touchesTheWalls)
	log.Println(mostAwesomeIndex)
	log.Println(highestCount)
	log.Println(totalLessThanCount)

}
