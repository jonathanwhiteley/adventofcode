package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type step struct {
	label        string
	requirements []string
	requiredFor  []string
	satisfied    bool
}

func getOrCreateStepFromMap(label string, stepMap map[string]step) (step, map[string]step) {

	if wantedStep, exists := stepMap[label]; exists {
		return wantedStep, stepMap
	} else {
		requirements := make([]string, 0)
		requiredFor := make([]string, 0)

		newStep := step{label: label, requirements: requirements, requiredFor: requiredFor, satisfied: false}

		stepMap[label] = newStep

		return newStep, stepMap
	}
}

func buildGraphFromFile(filename string) (map[string]step, []string) {
	graph := make(map[string]step)
	labels := make([]string, 0)

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		fields := strings.Fields(line)

		labelA := fields[1]
		labelB := fields[7]

		if !doesListContainString(labels, labelA) {
			labels = append(labels, labelA)
		}

		if !doesListContainString(labels, labelB) {
			labels = append(labels, labelB)
		}

		firstStep, graphA := getOrCreateStepFromMap(labelA, graph)
		nextStep, graphB := getOrCreateStepFromMap(labelB, graphA)

		graph = graphB

		firstStep.requirements = append(firstStep.requirements, labelB)
		nextStep.requiredFor = append(nextStep.requiredFor, labelA)

		graph[firstStep.label] = firstStep
		graph[nextStep.label] = nextStep

		// log.Println("firstStep.requirements:", firstStep.requirements)
		// log.Println("nextStep.requiredFor", nextStep.requiredFor)
	}

	return graph, labels
}

func doesListContainString(list []string, myString string) bool {
	for _, listItem := range list {
		if listItem == myString {
			return true
		}
	}

	return false
}

func checkIfSatisfied(chain string, stepToCheck step) bool {

	log.Println(chain, "contains", stepToCheck.requiredFor, "?")

	satisfied := true

	for _, req := range stepToCheck.requiredFor {
		if !strings.Contains(chain, req) {
			satisfied = false
		}
	}

	return satisfied
}

func checkIfSatisfiedArray(chain []string, stepToCheck step) bool {

	satisfied := true

	for _, req := range stepToCheck.requiredFor {
		if !doesListContainString(chain, req) {
			satisfied = false
		}
	}

	return satisfied
}

func processCurrentLevel(levelSteps []string, currentChain string, graph map[string]step) (string, []string) {
	nextLevel := make([]string, 0)

	sort.Strings(levelSteps)

	output := currentChain

	for _, stepLabel := range levelSteps {
		currentStep := graph[stepLabel]
		if checkIfSatisfied(currentChain, currentStep) {
			if !strings.Contains(output, stepLabel) {
				output += stepLabel
			}

			for _, req := range currentStep.requirements {
				log.Println(req)
				if !doesListContainString(nextLevel, req) {
					nextLevel = append(nextLevel, req)
				}
			}
		}
	}

	log.Println(nextLevel)

	return output, nextLevel
}

func main() {
	log.Println("Day 7")

	graph, labels := buildGraphFromFile(os.Args[1])

	sort.Strings(labels)

	labelValue := make(map[string]int)

	for index, label := range labels {
		labelValue[label] = (index + 1)
	}

	chain := ""

	for len(chain) < len(labels) {

		for _, label := range labels {

			if !strings.Contains(chain, label) {
				if checkIfSatisfied(chain, graph[label]) {
					chain += label
					break
				}
			}
		}
	}

	log.Println(chain)

	// var firstStep step

	// log.Println(len(graph))

	// for label, currentStep := range graph {
	// 	// log.Println("Label ", label)
	// 	// log.Println("Requires ", currentStep.requirements)
	// 	// log.Println("Required by ", currentStep.requiredFor)

	// 	if len(currentStep.requiredFor) == 0 {
	// 		log.Println(label, " has no depenecises")

	// 		firstStep = currentStep
	// 	}
	// }

	// log.Println("Start at ", firstStep.label)

	// currentLevel := make([]string, 1)
	// currentLevel[0] = firstStep.label

	// processChain := ""

	// for len(currentLevel) > 0 {
	// 	nextChain, nextLevel := processCurrentLevel(currentLevel, processChain, graph)

	// 	processChain = nextChain
	// 	currentLevel = nextLevel
	// }

	// log.Println(processChain)

	workOrder := make([]string, len(chain))
	workComplete := make([]string, 0)

	for index, work := range chain {
		workOrder[index] = string(work)
	}

	workers, _ := strconv.Atoi(os.Args[2])

	offset, _ := strconv.Atoi(os.Args[3])

	workerCounter := make([]int, workers)

	workerCurrentWorking := make(map[int]string)

	timeCount := 0

	for len(workComplete) < len(chain) || timeCount < 15 {
		// for timeCount < 15 {
		for index, workerCount := range workerCounter {
			if workerCount > 1 {
				workerCounter[index]--
			} else if workerCount == 1 {
				workerCounter[index]--
				workComplete = append(workComplete, workerCurrentWorking[index])

				for windex, work := range workOrder {

					if checkIfSatisfiedArray(workComplete, graph[work]) {
						workOrder = append(workOrder[:windex], workOrder[windex+1:]...)

						workerCurrentWorking[index] = work
						workerCounter[index] = offset + labelValue[work]

						break
					}
				}
			} else {
				// //check head of work order
				// if len(workOrder) > 0 {
				// 	workOrderLabel := workOrder[0]

				// 	if checkIfSatisfiedArray(workComplete, graph[workOrderLabel]) {
				// 		log.Println("Assiging ", workOrderLabel, " to work no. ", index, "with value ", (offset + labelValue[workOrderLabel]))

				// 		workerCurrentWorking[index] = workOrderLabel
				// 		workOrder = workOrder[1:]
				// 		workerCounter[index] = offset + labelValue[workOrderLabel]
				// 	}
				// }

				//Find next doable task

				for windex, work := range workOrder {

					if checkIfSatisfiedArray(workComplete, graph[work]) {
						workOrder = append(workOrder[:windex], workOrder[windex+1:]...)

						workerCurrentWorking[index] = work
						workerCounter[index] = offset + labelValue[work]

						break
					}
				}
			}
		}

		log.Println(timeCount, workOrder)

		timeCount++
	}

	log.Println(timeCount)

}
