package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type dataUnit struct {
	metaData   []int
	childNodes []dataUnit
	length     int
}

func buildLineInput(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		lines = append(lines, scanner.Text())
	}

	return lines
}

func findDataUnitAtOffset(dataValues []int, offset int) dataUnit {

	childCount := dataValues[offset]

	metaDataCount := dataValues[offset+1]

	currentDataUnit := dataUnit{childNodes: make([]dataUnit, childCount), metaData: make([]int, metaDataCount), length: 0}

	currentOffset := offset + 2

	for i := 0; i < childCount; i++ {
		currentChild := findDataUnitAtOffset(dataValues, currentOffset)
		currentDataUnit.childNodes[i] = currentChild
		currentOffset += currentChild.length
	}

	for i := 0; i < metaDataCount; i++ {
		currentDataUnit.metaData[i] = dataValues[currentOffset]
		currentOffset++
	}

	currentDataUnit.length = currentOffset - offset

	return currentDataUnit
}

func findMetaDataCount(currentDataUnit dataUnit) int {
	total := 0

	for _, childNode := range currentDataUnit.childNodes {
		total += findMetaDataCount(childNode)
	}

	for _, metaDatum := range currentDataUnit.metaData {
		total += metaDatum
	}

	return total
}

func findSecondMetaDataCount(currentDataUnit dataUnit) int {
	total := 0

	if len(currentDataUnit.childNodes) == 0 {
		for _, metaDatum := range currentDataUnit.metaData {
			total += metaDatum
		}
	} else {
		for _, metaDatum := range currentDataUnit.metaData {
			myIndex := metaDatum - 1
			if myIndex < len(currentDataUnit.childNodes) {
				total += findSecondMetaDataCount(currentDataUnit.childNodes[myIndex])
			}
		}
	}

	return total
}

func main() {

	input := buildLineInput(os.Args[1])

	if len(input) != 1 {
		log.Fatal("Not only 1 line of input")
	}

	dataValues := strings.Fields(input[0])

	data := make([]int, len(dataValues))

	for index, datum := range dataValues {
		datumAsInt, _ := strconv.Atoi(datum)

		data[index] = datumAsInt
	}

	offset := 0

	topLevel := make([]dataUnit, 0)

	for offset < len(data) {
		currentDataUnit := findDataUnitAtOffset(data, offset)
		offset += currentDataUnit.length
		topLevel = append(topLevel, currentDataUnit)
	}

	log.Println(len(topLevel))

	total := 0

	for _, currentDataUnit := range topLevel {
		total += findMetaDataCount(currentDataUnit)
	}

	log.Println(total)

	secondCount := 0

	for _, currentDataUnit := range topLevel {
		secondCount += findSecondMetaDataCount(currentDataUnit)
	}

	log.Println(secondCount)

}
