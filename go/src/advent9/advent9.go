package main

import (
	"container/ring"
	"log"
	"os"
	"strconv"
)

func playGame(players int, lastMarble int) int {
	circle := ring.New(1)
	circle.Value = 0

	playerScore := make([]int, players)

	for i := 1; i <= lastMarble; i++ {

		if i%23 == 0 {
			player := i % players

			circle = circle.Move(-8)

			removed := circle.Unlink(1)
			playerScore[player] += i + removed.Value.(int)
			circle = circle.Move(1)

		} else {

			circle = circle.Move(1)
			nextChain := ring.New(1)
			nextChain.Value = i

			circle.Link(nextChain)
			circle = circle.Move(1)
		}

	}

	highestScore := 0

	for _, score := range playerScore {
		if score > highestScore {
			highestScore = score
		}
	}

	return highestScore
}

func main() {

	players, _ := strconv.Atoi(os.Args[1])
	lastMarble, _ := strconv.Atoi(os.Args[2])

	log.Println(playGame(players, lastMarble))
}
